from rest_framework import serializers

from django_monstack_models.django_monstack_models.models import CollectServicesData


class CollectServicesSerializer(serializers.ModelSerializer):
    class Meta:
        model = CollectServicesData
        fields = ("service_id", "service_name")
